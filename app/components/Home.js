// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import routes from '../constants/routes';
import styles from './Home.css';
import { initialiseApp } from '@maidsafe/safe-node-app';
const app = require('electron').remote.app;

type Props = {};
const customExecPath = [process.execPath, app.getAppPath()];

export default class Home extends Component<Props> {
  props: Props;
  componentDidMount() {
    console.log("Component mounted")
    this.sendAuthRequest()
  }
  async sendAuthRequest() {
    console.log('Authorising SAFE application...');
    const appInfo = {
      name: 'SAFEmsg',
      id: 'com.mathcody.safemsg-app',
      version: '0.0.0',
      vendor: 'Math & Cody Ltd.',
      bundle: 'com.github.electron',
      customExecPath
    };

    const opts = {
    forceUseMock: true
  };

  const safeApp = await initialiseApp(appInfo, null, opts);
  const authUri = await safeApp.auth.genAuthUri({});
  await safeApp.auth.openUri(authUri);
}
  render() {
    return (
      <div className={styles.container} data-tid="container">
        <h2>Home</h2>
        <Link to={routes.COUNTER}>to Counter</Link>
      </div>
    );
  }
}
